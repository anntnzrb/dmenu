# annt's [dmenu](https://tools.suckless.org/dmenu/)'s custom build

```markdown
# dmenu - dynamic menu

dmenu is an efficient dynamic menu for X
```

> **suckless' [dmenu git repo](https://git.suckless.org/dmenu/file/README.html)**

## table of contents

- [patches](#patches)
- [installation](#installation)

## patches

- [border](https://tools.suckless.org/dmenu/patches/border/)
- [center](https://tools.suckless.org/dmenu/patches/center/)
- [hightlight](https://tools.suckless.org/dmenu/patches/highlight/)
- [line-height](https://tools.suckless.org/dmenu/patches/line-height/)
- [numbers](https://tools.suckless.org/dmenu/patches/numbers/)
- [password](https://tools.suckless.org/dmenu/patches/password/)

## installation

1. `git clone` this repo: (alternatively download manually)

   ```sh
   $ git clone 'https://gitlab.com/anntnzrb/suckless-collection.git'
   ```

2. `cd` into corresponding directory & install with `make`:

   ```sh
   # make install
   ```
